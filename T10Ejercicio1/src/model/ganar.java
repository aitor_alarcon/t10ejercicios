package model;

import exceptions.expectionsNumAleatorio;
import views.vistas;

public class ganar {

	// Creo el m�todo para mostrar si el usuario ha ganado
	public static void ganar() {
		
		// Creo el int del num introducido
		int numIntroducido;
		
		// instancio el numero aleatorio
		int numAleatorio = numeroAleatorio.numAleatorio(1, 500);
		
		// Creo el boolean ganar inicializado a false
		boolean ganar = false;
		
		// Creo el contador de intentos
		int intentos = 1;
		
		// Creo el Do While que contar� el n�mero de intentos y saldr� cuando el 
		// jugador gane la partida
		do {
			
			numIntroducido = expectionsNumAleatorio.exceptionNumAleatorio();
			
			if (adivinarNumero.menorMayorIgual(numIntroducido, numAleatorio)) {
				System.out.println("Numero intentos = " + intentos);
				ganar = true;
			}
			
			intentos++;
			
		} while (ganar == false);
		
	}
}
