package model;

import views.vistas;

public class adivinarNumero {
	
	// Instancio la clase vistas y llamo al m�todo preguntar numero
	int numIntroducido = vistas.preguntarNumero();
	
	// Instancio la clase numeroAleatorio y llamo al m�todo numAleatorio para crear un n�mero aleatorio
	// Entre 1 y 500
	int numAleatorio = numeroAleatorio.numAleatorio(1, 500);
	
	// Creo el metodo para comprobar numero introducido es mayor, menor o igual
	public static boolean menorMayorIgual(int numIntroducido, int numAleatorio) {
		
		if(numIntroducido > numAleatorio) {
			System.out.println("El n�mero introducido es mayor que el que buscas");
			System.out.println("prueba otra vez");
			return false;
		}
		else if (numIntroducido < numAleatorio) {
			System.out.println("El n�mero introducido es menor que el que buscas");
			System.out.println("prueba otra vez");
			return false;
		}
		else {
			System.out.println("Enhorabuena! has acertado el n�mero aleatorio");
			return true;
		}
		
	}
	
	
}
