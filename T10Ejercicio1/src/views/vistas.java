package views;

import java.util.Scanner;

public class vistas {

	// Creo el m�todo para mostrar las views y preguntar el n�mero que va a introducir
	// En ese momento
	public static int preguntarNumero() {
		
		Scanner teclado = new Scanner(System.in);
		
		System.out.println("Introduce un n�mero");
		int num = teclado.nextInt();
		
		return num;
	}
}
