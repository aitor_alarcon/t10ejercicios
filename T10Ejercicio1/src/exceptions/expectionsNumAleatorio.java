package exceptions;

import java.util.InputMismatchException;

import views.vistas;

public class expectionsNumAleatorio {

	// Creo el m�todo de exception
	public static int exceptionNumAleatorio() {
		
		// Inicializo el numero introducido a 0
		int numIntroducido = 0;
		
		// Creo el Try Catch
		try {
				
			// Instancio la clase vistas y llamo al m�todo de preguntar Numero
			numIntroducido = vistas.preguntarNumero();
		
			// Y si el n�mero introducido en preguntar n�mero da este error
		} catch (InputMismatchException e) {
			// Muestra esto
				System.out.println("No se ha introducido un n�mero");
		}
			
		// Retorna el n�mero introducido
		return numIntroducido;
		
	}
}
