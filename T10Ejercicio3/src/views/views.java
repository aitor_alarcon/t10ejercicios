package views;

import dto.numeroRandom;


public class views {

	public int mostrar() {
		
		// Instancio la clase numero random
		numeroRandom numeroGenerado = new numeroRandom();
		
		// Lo guardo 
		int numeroRandom = numeroGenerado.generarNumeroAleatorio();
		
		// Y lo muestro
		System.out.println("Generando n�mero aleatorio...");
		System.out.println("El numero aleatorio generado es: " + numeroRandom);
		
		// Retorno el n�mero random
		return numeroRandom;
	}
	
}
