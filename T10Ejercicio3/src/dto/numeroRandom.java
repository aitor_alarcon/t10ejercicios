package dto;

import java.util.Random;

public class numeroRandom {

	// Creo el m�todo para generar un n�mero aleatorio con la clase random
	public int generarNumeroAleatorio() {
		
		// Instancio la clase random
		Random rng = new Random();
		
		// Creo el n�mero random
		int numeroGenerado = rng.nextInt(999)+1;
		
		// Retorno el n�mero random
		return numeroGenerado;
	}
}
