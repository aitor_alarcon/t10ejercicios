package dto;

// Creo mi excepcion reutilizada del ejercicio2
public class miException extends Exception{

	private int codigoException;

	public miException(int codigoException) {
		this.codigoException = codigoException;
	}
	
	public String exceptionCustom() {
		
		String mensajeError = "";
		
		if(codigoException == 001) {
			mensajeError = "Es par";
		}
		else if(codigoException == 002) {
			mensajeError = "Es impar";
		}
		
		return mensajeError;
		
	}
	

}
