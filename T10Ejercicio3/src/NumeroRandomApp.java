import dto.miException;
import views.views;

public class NumeroRandomApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Instancio las vistas
		views programa = new views();
		
		// Para mostrar las vistas que pide el enunciado
		int numeroRandom = programa.mostrar();
		
		// Hago el try catch que he reutilizado del dos
		try {
			
			if(numeroRandom % 2 == 0) {
				throw new miException(001);
			}
			else {
				throw new miException(002);
			}
		} catch (miException msg) {
			
			System.out.println(msg.exceptionCustom());
		}
		
	}

}
