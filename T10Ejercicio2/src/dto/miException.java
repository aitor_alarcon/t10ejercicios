package dto;

public class miException extends Exception{

	// Atributos
	private int codigoException;

	// Constructores
	public miException(int codigoException) {
		this.codigoException = codigoException;
	}
	
	// Creo el m�todo para crear los mensajes que dara mi excepcion
	public String exceptionCustom() {
		
		String mensajeError = "";
		
		// Creo los mensajes dependiendo de un if
		if(codigoException == 001) {
			mensajeError = "Error 001 el n�mero introducido es par";
		}
		else if(codigoException == 002) {
			mensajeError = "Error 002 el n�mero introducido es impar";
		}
		
		// Devuelvo el mensaje
		return mensajeError;
	}
}
