import java.util.Scanner;

import dto.miException;
import views.pedirNumero;

public class miExceptionApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		// Instancio para pedir el n�mero
		pedirNumero num = new pedirNumero();
				
		// Creo un int para poder llamar al metodo y pedir el n�mero
		int numeroIntroducido = num.pedirNumero();
		
		// Creo el try catch que dara la excepcion personalizada
		try {
				
			// Que si el n�mero es par da esto
			if (numeroIntroducido % 2 == 0) {
				throw new miException(001);
			}
			// y si no esto
			else {
				throw new miException(002);
			}
			
			// Muestra la excepcion personalizada
		} catch (miException msg) {
			// TODO: handle exception
			System.out.println(msg.exceptionCustom());
		}
	}

}
