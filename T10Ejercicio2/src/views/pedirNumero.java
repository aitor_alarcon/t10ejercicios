package views;

import java.util.Scanner;

public class pedirNumero {

	// Creo este m�todo para pedir el n�mero que se va a introducir
	public static int pedirNumero() {
		
		Scanner teclado = new Scanner(System.in);
		
		System.out.println("Introduce un n�mero");
		int numero = teclado.nextInt();
		
		return numero;
	}
}
