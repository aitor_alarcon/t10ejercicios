package views;

import java.util.Scanner;

import dto.calculadora;
import exceptions.excepcionesCalculo;
import exceptions.miExcepcionCalculos;

public class pedirNumeros {

	// Creo la vista para pedir el numero 1
	public int num1() {
		
		Scanner teclado = new Scanner(System.in);
		
		System.out.println("Introduce el primer n�mero");
		int num1 = teclado.nextInt();
		
		return num1;
	}
	
	// Creo la vista para pedir el numero 2
	public int num2() {
		
		Scanner teclado = new Scanner(System.in);
		
		System.out.println("Introduce el segundo n�mero");
		int num2 = teclado.nextInt();
		
		return num2;
	}
	
	// Creo la vista para pedir al usuario que calculo quiere usar
	public void elegirCalculo() throws miExcepcionCalculos {
		
		Scanner teclado = new Scanner(System.in);
		
		System.out.println("Que c�lculo quieres hacer?");
		System.out.println("1. suma, 2. resta, 3. multiplicaci�n, 4. potencia, 5. ra�z cuadrada");
		System.out.println("6. ra�z c�bica, 7. Division");
		int eleccion = teclado.nextInt();
		
		excepcionesCalculo calculo = new excepcionesCalculo();
		
		// Y hago el c�lculo de lo que haya elegido
		switch (eleccion) {
		case 1:
			int num1 = num1();
			
			int num2 = num2();
			
			calculo.exceptionSuma(num1, num2);
			break;

		case 2:
			int num3 = num1();
			
			int num4 = num2();
			
			calculo.exceptionResta(num3, num4);
			
			break;
			
		case 3:
			int num5 = num1();
			
			int num6 = num2();
			
			calculo.exceptionMultiplicacion(num5, num6);
			
			break;
			
		case 4:
			int num7 = num1();
			System.out.println("(Base)");
			int num8 = num2();
			System.out.println("(Exponente)");
			calculo.exceptionPotencia(num7, num8);
			
			break;
			
		case 5:
			int num9 = num1();
			
			calculo.exceptionRaizCuadrada(num9);
			
			break;
			
		case 6:
			int num10 = num1();
			
			calculo.exceptionRaizCubica(num10);
			
			break;
		
		case 7:
			int num11 = num1();
			
			int num12 = num2();
			
			calculo.exceptionDivision(num11, num12);
		default:
			break;
		}
	}
}
