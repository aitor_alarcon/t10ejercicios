package dto;

public class calculadora {

	// Creo todos los m�todos que se encargaran de los calculos
	public int suma(int num1, int num2) {
		int suma = num1 + num2;
		
		return suma;
	}
	
	public int resta(int num1, int num2) {
		int resta = num1 + num2;
		
		return resta;
	}
	
	public int multiplicacion(int num1, int num2) {
		int multplicacion = num1 * num2;
		
		return multplicacion;
	}
	
	public int potencia(int num1, int num2) {
		int potencia = (int) Math.pow(num1, num2);
		
		return potencia;
	}
	
	public double raizCuadrada(int num1) {
		double raizCuadrada = Math.sqrt(num1);
		
		return raizCuadrada;
	}
	
	public double raizCubica(int num1) {
		double raizCubica = Math.cbrt(num1);
		
		return raizCubica;
	}
	
	public double division(int num1, int num2) {
		double division = num1 / num2;
		
		return division;
	}
}
