package exceptions;

import dto.calculadora;

public class excepcionesCalculo {

	// Instancio la clase que hace de calculadora
	calculadora calculo = new calculadora();
	
	// Creo todos los metodos que comprobaran que los n�meros introducidos son correctos
	public int exceptionSuma(int num1, int num2) throws miExcepcionCalculos{
		
		int resultado = 0;
		
		// Creo el try catch
		try {
			
			// si el los n�meros introducidos son negativos salta la excepcion
			if (num1 < 0 || num2 < 0) {
				throw new miExcepcionCalculos(1);
			}
			
			// Muestro el resultado
			System.out.println("Resultado = " + (resultado = calculo.suma(num1, num2)));
			
		} catch (miExcepcionCalculos msg) {
			// TODO: handle exception
			System.out.println(msg.mensajeError());
		}
		
		return resultado;
	}
	
	public int exceptionResta(int num1, int num2) {
		
		int resultado = 0;
		
		try {
			
			if (num1 < 0 || num2 < 0) {
				throw new miExcepcionCalculos(1);
			}
			
			System.out.println("Resultado = " + (resultado = calculo.resta(num1, num2)));
			
		} catch (miExcepcionCalculos msg) {
			// TODO: handle exception
			System.out.println(msg.mensajeError());
		}
		
		return resultado;
	}
	
	public int exceptionMultiplicacion(int num1, int num2) {
		
		int resultado = 0;
		
		try {
			
			if (num1 < 0 || num2 < 0) {
				throw new miExcepcionCalculos(1);
			}
			
			System.out.println("Resultado = " + (resultado = calculo.multiplicacion(num1, num2)));
			
		} catch (miExcepcionCalculos msg) {
			// TODO: handle exception
			System.out.println(msg.mensajeError());
		}
		
		return resultado;
	}
	
	public int exceptionPotencia(int num1, int num2) {
		
		int resultado = 0;
		
		try {
			
			if (num1 < 0 || num2 < 0) {
				throw new miExcepcionCalculos(1);
			}
			
			System.out.println("Resultado = " + (resultado = calculo.potencia(num1, num2)));
			
		} catch (miExcepcionCalculos msg) {
			// TODO: handle exception
			System.out.println(msg.mensajeError());
		}
		
		return resultado;
	}
	
	public double exceptionRaizCuadrada(int num1) {
		
		double resultado = 0;
		
		try {
			
			if (num1 < 0) {
				throw new miExcepcionCalculos(1);
			}
			
			System.out.println("Resultado = " + (resultado = calculo.raizCuadrada(num1)));
			
		} catch (miExcepcionCalculos msg) {
			// TODO: handle exception
			System.out.println(msg.mensajeError());
		}
		
		return resultado;
	}
	
	public double exceptionRaizCubica(int num1) {
		
		double resultado = 0;
		
		try {
			
			if (num1 < 0) {
				throw new miExcepcionCalculos(1);
			}
			
			System.out.println("Resultado = " + (resultado = calculo.raizCubica(num1)));
			
		} catch (miExcepcionCalculos msg) {
			// TODO: handle exception
			System.out.println(msg.mensajeError());
		}
		
		return resultado;
	}
	
	public double exceptionDivision(int num1, int num2) {
		
		double resultado = 0;
		
		try {
			
			if (num1 < 0 || num2 < 0) {
				throw new miExcepcionCalculos(1);
			}
			
			System.out.println("Resultado = " + (resultado = calculo.division(num1, num2)));
			
		} catch (miExcepcionCalculos msg) {
			// TODO: handle exception
			System.out.println(msg.mensajeError());
		}
		
		return resultado;
	}
}
