package exceptions;

public class miExcepcionCalculos extends Exception{

	private int codigoExcepcion;
	
	
	public miExcepcionCalculos() {
		this.codigoExcepcion = 0;
	}


	public miExcepcionCalculos(int codigoExcepcion) {
		this.codigoExcepcion = codigoExcepcion;
	}

	// Creo mi excepcion personalizada que dara error
	public String mensajeError() {
		
		String mensaje = "";
		
		switch (codigoExcepcion) {
		case 1:
			
			mensaje = "Introduce un nmero mayor de cero";
			break;
			
		}
		
		return mensaje;
		
	}

}
