package dto;

import java.util.Random;

public class Password {

	// Atributos
		private static int longitud;
		
		private static String contraseña;

		// Constructores
		public Password() {
			this.longitud = 8;
			this.contraseña = generarContraseña(this.longitud);
		}

		public Password(int longitud) {
			this.longitud = longitud;
			this.contraseña = generarContraseña(this.longitud);
			
		}
		
		// Creo el método para que genere la contraseña aleatoria
		public static String generarContraseña (int longitud) {
			
			String contraseña = "";
			
			Random rng = new Random();
			
			for (int i = 0; i < longitud; i++) {
				contraseña += (char) (rng.nextInt(91) + 65);
			}
			
			return contraseña;
		}
		
		// Creo el método para comprobar si la contraseña es buena o no
		public boolean esFuerte() {
			
			int numeros = 0;
			
			int minusculas = 0;
			
			int mayusculas = 0;
			
			// Hago un bucle para recorrer las contraseñas y contar las mayus, minus y numeros
			for (int i = 0; i < longitud; i++) {
				
				if (contraseña.charAt(i) >= 97 && contraseña.charAt(i) <= 122) {
					minusculas++;
				}
				else if (contraseña.charAt(i) >= 65 && contraseña.charAt(i) <= 90) {
					mayusculas++;
				}
				else {
					numeros++;
				}
			}
			
			// Hago el if para comprobar si la contraseña es segura o no
			if (mayusculas > 2 && minusculas > 1 && numeros > 5) {
				return true;
			}
			else {
				return false;
			}
		}

		// Getters y setters
		public int getLongitud() {
			return longitud;
		}

		public void setLongitud(int longitud) {
			this.longitud = longitud;
		}

		public String getContraseña() {
			return contraseña;
		}
		
		
		
}
