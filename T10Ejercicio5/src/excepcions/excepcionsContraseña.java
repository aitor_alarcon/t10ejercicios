package excepcions;

import java.util.InputMismatchException;

import views.views;

public class excepcionsContraseña {

	// Creo la clase para validar el número introducido
	public static int validarNumero() {
		
		int num = 0;
		
		// Creo el try catch
		try {
			
			num = views.preguntarTamaño();
			
			// Si lo que ha introducido no es un número le digo lo siguiente
		}catch(InputMismatchException msg){
			System.out.println("No has introducido un número");
		}
		
		return num;
	}
}
