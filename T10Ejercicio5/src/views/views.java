package views;

import java.util.Scanner;

import dto.Password;
import excepcions.excepcionsContraseña;

public class views {

	// Método para preguntar el tamaño de los arrays
	public static int preguntarTamaño() {
		
		int num = 0;
		
		Scanner teclado = new Scanner(System.in);
		
		num = teclado.nextInt();
		
		return num;
	}

	// Este es el método que muestra lo que pide el ejercicio
	public static void crearContraseña() {
		
		// PRegunto el tamaño de los arrays
		System.out.println("Que tamaño le vas a dar al array de contraseñas?");
		int tamañoArray = excepcionsContraseña.validarNumero();
		
		// Creo los arrays
		Password[] arrayPasswords = new Password[tamañoArray];
		boolean[] arrayBoolean = new boolean[tamañoArray];
		
		// Pregunto cuanto queiere que midan las contraseñas
		System.out.println("Cuantos carácteres quieres que tengan las contraseñas");
		int caracteres = excepcionsContraseña.validarNumero();
		
		for (int i = 0; i < arrayPasswords.length; i++) {
			arrayPasswords[i] = new Password(caracteres);
			arrayBoolean[i] = arrayPasswords[i].esFuerte();
			System.out.println("La contraseña " + arrayPasswords[i] + " es: " + arrayBoolean[i] + " (true = buena, false = mala)");
		}
	}
	
}
